from flask import Flask
from flask import render_template

app = Flask(__name__)
@app.route('/trivia.html')
def index():
    return render_template('trivia.html')

@app.route("/<path:url>")
def invalid_file(url):
    length = url.split('/')
    checked = url.split('.')
    if len(length) > 2:
        return render_template('403.html'),403
    elif checked[1] == ('html' or 'css'):
        return render_template('404.html'),404
    else:
        return render_template('403.html'),403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0',port=5000)
